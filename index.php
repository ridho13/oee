<?php
$judulhalaman = 'Overall Equipment Effectiveness';
include("header.php");

$plantTwo = array(
  array("m21", "H005", "6", "5"),
  array("m22", "L010", "12", "5"),
  array("m23", "M003", "18", "5"),
  array("m24", "L016", "6", "15")
);

$plantOne = array(
  array("m11", "P002", "6", "15")
);
?>
<div class="container-fluid">

  <!-- Main Content Area -->
  <div class="row">
    <div class="col-xl-3 col-lg-3">

    </div>
  </div>
  <div class="row">
    <!-- Plan -->
    <div class="col-xl-5 col-lg-5">
      <div class="card shadow mb-4">
        <a class="d-block card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Plant 2</h6>
        </a>
        <div class="collapse show" id="card-plant-two">
          <div class="card-body">
            <div id="plant-two">
              <?php foreach ($plantTwo as $mesin) : ?>
                <div id="<?= $mesin[0] ?>" style="margin-top: <?= $mesin[2] ?>vw; margin-left: <?= $mesin[3] ?>vw; position: absolute;">
                  <svg class="object-box">
                    <a xlink:href="oee-details.php?m=<?= $mesin[1] ?>" target="_top">
                      <rect x="0" y="0" class="object-box" stroke="#717380" stroke-width="3px" fill="white" id="machine-<?= $mesin[1] ?>" />
                      <text x="50%" y="50%" fill="black" dominant-baseline="middle" text-anchor="middle" id="text-machine-<?= $mesin[1] ?>"><?= $mesin[1] ?></text>
                    </a>
                  </svg>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-5 col-lg-5">
      <div class="card shadow mb-4">
        <a class="d-block card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Plant 1</h6>
        </a>
        <div class="collapse show" id="card-plant-one">
          <div class="card-body">
            <div id="plant-one">
              <?php foreach ($plantOne as $mesin) : ?>
                <div id="<?= $mesin[0] ?>" style="margin-top: <?= $mesin[2] ?>vw; margin-left: <?= $mesin[3] ?>vw; position: absolute;">
                  <svg class="object-box">
                    <a xlink:href="oee-details.php?m=<?= $mesin[1] ?>" target="_top">
                      <rect x="0" y="0" class="object-box" stroke="#717380" stroke-width="3px" fill="white" id="machine-<?= $mesin[1] ?>" />
                      <text x="50%" y="50%" fill="black" dominant-baseline="middle" text-anchor="middle" id="text-machine-<?= $mesin[1] ?>"><?= $mesin[1] ?></text>
                    </a>
                  </svg>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-2 col-lg-2">
      <div class="card shadow mb-4">
        <div class="collapse show" id="card-legend">
          <div class="card-body">
            <div id="card-legend-content">
              <a class="btn btn-icon-split" style="font-size: smaller;cursor: default;background-color: #f7f700;">
                <span class="text text-gray">Standby</span>
              </a><br />
              <a class="btn btn-success btn-icon-split" style="font-size: smaller;cursor: default;">
                <span class="text text-white">Run</span>
              </a><br />
              <a class="btn btn-warning btn-icon-split" style="font-size: smaller;cursor: default;">
                <span class="text text-white">Idle</span>
              </a><br />
              <a class="btn btn-light btn-icon-split" style="font-size: smaller;cursor: default;">
                <span class="text text-gray">Break</span>
              </a><br />
              <a class="btn btn-primary btn-icon-split" style="font-size: smaller;cursor: default;">
                <span class="text text-white">QC Caller</span>
              </a><br />
              <a class="btn btn-primary btn-icon-split red" style="font-size: smaller;cursor: default;">
                <span class="text text-white">Support Caller</span>
              </a><br />
              <a class="btn btn-danger btn-icon-split" style="font-size: smaller;cursor: default;">
                <span class="text text-white">Breakdown</span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <a href="parameter.php" class="btn btn-secondary btn-icon-split btn-lg btn-block">
        <span class="text text-white" style="font-size: small;width: 100%">Parameter Details</span>
      </a>
    </div>
  </div>
</div>
</div>

<?php include('footer.php') ?>
</div>
</div>

<!-- Scroll to Top -->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap -->
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>

<script>
  function updateStatus(mesin) {
    $.ajax({
      url: "machineStatus.php",
      type: "POST",
      dataType: "JSON",
      data: {
        mesin: mesin
      },
      success: function(data) {
        $newColor = "#000000";
        $newTextColor = "#FFFFFF";
        $newBorderColor = "#000000";
        switch (data.status) {
          case 'Setup Time':
            $newColor = "#f7f700";
            $newTextColor = "#978a99";
            break;
          case 'Run Time':
            $newColor = "#1cc88a";
            break;
          case 'Idle Time':
            $newColor = "#f6c23e";
            break;
          case 'Normal Break Time':
          case 'Urgent Break Time':
            $newColor = "#f8f9fc";
            $newTextColor = "#978a99";
            break;
          case 'QC Call Time':
            $newColor = "#4e73df";
            break;
          case 'Support Call Time':
            $newColor = "#4e73df";
            $newBorderColor = "#978a99";
            break;
          case 'Breakdown Time':
            $newColor = "#e74a3b";
            break;
          default:
            $newColor = "#000000";
        }
        $('#machine-' + mesin).css("fill", $newColor);
        $('#machine-' + mesin).css("stroke", $newBorderColor);
        $('#text-machine-' + mesin).css("fill", $newTextColor);
      }
    });
  }

  function updateTime() {
    var datePlace = document.getElementById("updated-date");

    var now = new Date(),
      h = now.getHours(),
      m = now.getMinutes();

    var format = {
      day: "numeric",
      month: "long",
      year: "numeric"
    }

    if (h < 10) h = '0' + h;
    if (m < 10) m = '0' + m;

    var timeNow = 'Diperbarui pada ' + now.toLocaleDateString("id", format) + ' | ' + h + ':' + m;

    datePlace.innerHTML = timeNow;
  }

  function updateAll() {
    updateStatus('H005');
    updateStatus('L010');
    updateStatus('M003');
    updateStatus('L016');
    updateStatus('P002');
    updateTime();
  }

  updateAll();

  setInterval(() => {
    updateAll();
  }, 60000);
</script>
</body>

</html>
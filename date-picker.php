<?php

if (isset($_POST['date-start']) && isset($_POST['date-end'])) {
  $_SESSION['start'] = $_POST['date-start'];
  $_SESSION['end'] = $_POST['date-end'];
}

if (!isset($_SESSION['start']) && !isset($_SESSION['end'])) {
  $_SESSION['start'] = date('Y-m-d', strtotime('-7 days'));
  $_SESSION['end'] = date('Y-m-d');
}
?>
<div class="row justify-content-end">
  <div class="col-xl-12 col-lg-12">
    <!-- Date Picker -->
    <div class="card shadow mb-4">
      <a class="d-block card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tanggal</h6>
      </a>
      <div class="card-body">
        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
          <div class="input-group">
            <input class="textbox-n form-control bg-light border-0 small" type="date" id="date-start" name="date-start" value="<?php echo $_SESSION['start'] ?>">
            <input class="textbox-n form-control bg-light border-0 small ml-4" type="date" id="date-end" name="date-end" value="<?php echo $_SESSION['end'] ?>">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">
                <i class="fas fa-calendar fa-sm"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
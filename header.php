<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE-edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Halaman parameter details OEE Universal Tekno Reksajaya">
  <meta name="author" content="Fikri Haykal">

  <title><?php echo $judulhalaman . ' - Universal Tekno Reksajaya'; ?></title>

  <!-- External Fonts and CSS -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Local CSS -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Main Wrapper or Content Area -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- Top Navigation -->
        <nav class="row navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" style="height: 6rem;">

          <ul class="navbar-nav ml-5 d-sm-inline-block">
            <a href="/"><img src="img/index.png" style="width: 80px; height: auto;" /></a>
          </ul>

          <ul class="navbar-nav ml-auto">
            <h1 class="h3 mb-0 text-gray-800"><?php echo $judulhalaman; ?></h1>
          </ul>

          <ul class="navbar-nav ml-auto mr-5">
            <a class="btn btn-success btn-icon-split" style="cursor: default;">
              <span class="icon text-white-50">
                <i class="fas fa-clock"></i>
              </span>
              <span class="text text-white" id="updated-date">
                <?php
                setlocale(LC_ALL, 'IND');
                date_default_timezone_set('Asia/Makassar');
                echo "Diperbarui pada ";
                echo strftime('%d %B %Y | %H:%M');
                ?>
              </span>
            </a>
          </ul>
        </nav>
<?php
session_start();
$m = $_GET["m"];
if (strlen($m) != 4)
  header('Location: /');

$judulhalaman = $m . ' OEE Details ';
include("header.php");
include("function.php");

// Availability
// Rumus Availability = Working Time / (Grand total - StandBy)
// Grand Total = breakdown + idle + standby + qc + qc call + rest + running
$runningTime = availability($m, "Run Time");
$breakdownTime = availability($m, "Breakdown Time");
$idleTime = availability($m, "Idle Time");
$standByTime = availability($m, "Setup Time");
$qcCallTime = availability($m, "QC Call Time");
$normalBreakTime = availability($m, "Normal Break Time");
$grandTotal = $breakdownTime + $idleTime + $standByTime + $qcCallTime + $normalBreakTime + $runningTime;
$availability = $runningTime / ($grandTotal - $standByTime);

// Quality
$good = quality($m, "Product Good Time");
$total = quality($m, "New Product Time");
$quality = ($good / $total);

//die($quality);

// Performance 
// Rumus Performance 
// (Average standard Time x Actual WO Qty) / Running Time
$averageStdTime = 7;
$actualQty = 1;
$performance = ($averageStdTime * $actualQty) / $runningTime;

if (is_nan($availability) || !is_numeric($availability))
  $availability = 0.00001;
if (is_nan($quality) || !is_numeric($quality))
  $quality = 0.00001;
if (is_nan($performance) || !is_numeric($performance) || is_infinite($performance))
  $performance = 0.00001;

// Bar OEE
$bar = $availability * $quality * $performance * 100;
if (is_nan($bar) || !is_numeric($bar))
  $bar = 0;

//$bar = number_format(($availability * $quality * $performance * 10), 2, '.', '');

// MTTR
$breakdownTime_MTTR = availability($m, "Breakdown Time");
$frekuensiBreakDown = frekuensi($m, "Breakdown Time");
$MTTR = $breakdownTime_MTTR / $frekuensiBreakDown;

// MTBF
$MTBF = $runningTime / $frekuensiBreakDown;
?>
<div class="container-fluid">
  <!-- Main Content Area -->
  <?php include('date-picker.php') ?>
  <div class="row">
    <div class="col">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">OEE</h6>
        </div>
        <div class="card-body">
          <div class="progress mb-4">
            <div class="progress-bar" role="progressbar" style="width: <?= $bar ?>%" aria-valuenow="<?= $bar ?>" aria-valuemin="0" aria-valuemax="100">
              <span class="oee-percent"><?= intval($bar) ?>%</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <!-- Availability Pie Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Availability</h6>
        </div>
        <div class="collapse show" id="availabilityChartArea">
          <div class="card-body">
            <div class="chart-pie pt-4 pb-2">
              <canvas id="availability-pie-chart"></canvas>
            </div>
            <div class="mt-4 text-center small">
              <span class="mr-2">
                <i class="fas fa-circle text-primary"></i> Availability
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Performance Pie Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Performance</h6>
        </div>
        <div class="collapse show" id="performanceChartArea">
          <div class="card-body">
            <div class="chart-pie pt-4 pb-2">
              <canvas id="performance-pie-chart"></canvas>
            </div>
            <div class="mt-4 text-center small">
              <span class="mr-2">
                <i class="fas fa-circle text-primary"></i> Performance
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Quality Pie Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Quality</h6>
        </div>
        <div class="collapse show" id="qualityChartArea">
          <div class="card-body">
            <div class="chart-pie pt-4 pb-2">
              <canvas id="quality-pie-chart"></canvas>
            </div>
            <div class="mt-4 text-center small">
              <span class="mr-2">
                <i class="fas fa-circle text-primary"></i> Quality
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- MTBF Tab -->
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-s font-weight-bold text-primary mb-1">MTBF (Mean Time Between Failure)</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= fixedDate($MTBF) ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- MTTR Tab -->
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-s font-weight-bold text-primary mb-1">MTTR (Mean Time to Repair)</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= fixedDate($MTTR) ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-between">
    <div class="col-xl-2 col-lg-2 mb-4">
      <a href="index.php" class="btn btn-danger btn-icon-split btn-lg btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text text-white" style="width: 100%">Back</span>
      </a>
    </div>
    <div class="col-xl-2 col-lg-2 mb-4">
      <a href="#" class="btn btn-primary btn-icon-split btn-lg btn-block" data-toggle="modal" data-target="#signInModal">
        <span class="icon text-white-50">
          <i class="fas fa-cog"></i>
        </span>
        <span class="text text-white" style="width: 100%">Input Target</span>
      </a>
    </div>
  </div>
</div>
</div>

<?php include('footer.php') ?>
</div>
</div>

<div id="signInModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="row">
        <div class="col-lg-6 d-none d-lg-block bg-login-image" style="background-image: url('img/bg.jpeg');"></div>
        <div class="col-lg-6 align-items-center">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">Selamat Datang!</h1>
            </div>
            <form class="user" method="post" action="login.php">
              <div class="form-group">
                <input type="text" class="form-control form-control-user" id="username" name="utr-username" required placeholder="UserID">
              </div>
              <div class="form-group">
                <input type="password" class="form-control form-control-user" id="password" name="utr-password" required placeholder="Password">
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox small">
                  <input type="checkbox" class="custom-control-input" id="remember-password">
                </div>
              </div>
              <input type="submit" value="Masuk" name="login" class="btn btn-primary btn-user btn-block">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<!-- Bootstrap -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>

<!-- ChartJS -->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="js/doughnut-text.js"></script>

<!-- Main OEE Chart -->
<!-- <script>
  var availabilityChartX = document.getElementById("availability-pie-chart");
  var availabilityChart = new Chart(availabilityChartX, {
    type: 'doughnut',
    data: {
      labels: ["Availability", "Unavailable"],
      datasets: [{
        data: [80, 20],
        backgroundColor: ['#4e73df', '#1cc88a'],
        hoverBackgroundColor: ['#2e59d9', '#17a673'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '80%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 70,
    }
  });

  var performanceChartX = document.getElementById("performance-pie-chart");
  var performanceChart = new Chart(performanceChartX, {
    type: 'doughnut',
    data: {
      labels: ["Performance", "Unperformed"],
      datasets: [{
        data: [75, 25],
        backgroundColor: ['#4e73df', '#1cc88a'],
        hoverBackgroundColor: ['#2e59d9', '#17a673'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '75%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 70,
    }
  });

  var qualityChartX = document.getElementById("quality-pie-chart");
  var qualityChart = new Chart(qualityChartX, {
    type: 'doughnut',
    data: {
      labels: ["Quality", "Unqualified"],
      datasets: [{
        data: [85, 15],
        backgroundColor: ['#4e73df', '#1cc88a'],
        hoverBackgroundColor: ['#2e59d9', '#17a673'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '85%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 70,
    }
  });

  var aCtx = document.getElementById("availability-pie-chart").getContext("2d");
  var ax = availabilityChartX.width / 2;
  var ay = availabilityChartX.height / 2;
  aCtx.textAlign = 'center';
  aCtx.textBaseline = 'middle';
  aCtx.font = '14px Nunito'
  aCtx.fillStyle = '#5A5C69'
  aCtx.fillText("80%", ax, ay);
</script>
</body> -->

<!-- Main OEE Chart -->
<script>
  var availabilityChartX = document.getElementById("availability-pie-chart");
  var availabilityChart = new Chart(availabilityChartX, {
    type: 'doughnut',
    data: {
      labels: ["Availability", ""],
      datasets: [{
        data: [<?php echo $availability ?>, <?php echo (1.0 - $availability) ?>],
        backgroundColor: ['#2e59d9', '#ffffff'],
        hoverBackgroundColor: ['#2e59d9', '#ffffff'],
        borderColor: "rgba(234, 236, 244, 1)",
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '<?php echo intval($availability * 100) ?>%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 50,
    }
  });

  var performanceChartX = document.getElementById("performance-pie-chart");
  var performanceChart = new Chart(performanceChartX, {
    type: 'doughnut',
    data: {
      labels: ["Performance", ""],
      datasets: [{
        data: [<?php echo $performance ?>, <?php echo (1.0 - $performance) ?>],
        backgroundColor: ['#2e59d9', '#ffffff'],
        hoverBackgroundColor: ['#2e59d9', '#ffffff'],
        borderColor: "rgba(234, 236, 244, 1)",
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '<?php echo intval($performance * 100) ?>%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 50,
    }
  });

  var qualityChartX = document.getElementById("quality-pie-chart");
  var qualityChart = new Chart(qualityChartX, {
    type: 'doughnut',
    data: {
      labels: ["Quality", ""],
      datasets: [{
        data: [<?php echo $quality ?>, <?php echo (1.0 - $quality) ?>],
        backgroundColor: ['#2e59d9', '#ffffff'],
        hoverBackgroundColor: ['#2e59d9', '#ffffff'],
        borderColor: "rgba(234, 236, 244, 1)",
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      elements: {
        center: {
          text: '<?php echo intval($quality * 100) ?>%',
          color: '#5A5C69',
          fontStyle: 'Arial',
          sidePadding: 20,
          minFontSize: 20,
          lineHeight: 36
        }
      },
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      },
      cutoutPercentage: 50,
    }
  });

  var aCtx = document.getElementById("availability-pie-chart").getContext("2d");
  var ax = availabilityChartX.width / 2;
  var ay = availabilityChartX.height / 2;
  aCtx.textAlign = 'center';
  aCtx.textBaseline = 'middle';
  aCtx.font = '14px Nunito'
  aCtx.fillStyle = '#5A5C69'
  aCtx.fillText("80%", ax, ay);
</script>
</body>

</html>
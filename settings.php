<?php
include("connection.php");

session_start();
include("header.php");
// // if(!isset($_SESSION['status'])){
//     header("location: sign-in.php");
// } else{
//     $judulhalaman = 'Settings';
//     include("header.php");
// }
?>
<div class="container-fluid">
  <!-- Main Content Area -->
  <div class="row">
    <div class="col-xl-12 col-lg-12">
      <!-- Settings Form Group -->
      <div class="card shadow mb-4">
        <a href="#settingsForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Input Target</h6>
        </a>
        <div class="collapse show" id="settingsForm">
          <div class="card-body">
            <div class="p-5">
              <form class="user" method="POST" action="settingsTarget.php">
                <div class="form-group">
                  <div class="text-s font-weight-bold text-uppercase text-primary mb-1">Mesin H005</div>
                  <input type="text" class="form-control form-control-user" style="border-radius: 0.5vw;" id="h005" name="h005" placeholder="Masukkan Target Mesin H005 (product/week)">
                </div>
                <div class="form-group">
                  <div class="text-s font-weight-bold text-uppercase text-primary mb-1">Mesin L010</div>
                  <input type="text" class="form-control form-control-user" style="border-radius: 0.5vw;" id="l010" name="l010" placeholder="Masukkan Target Mesin L010 (product/week)">
                </div>
                <div class="form-group">
                  <div class="text-s font-weight-bold text-uppercase text-primary mb-1">Mesin L016</div>
                  <input type="text" class="form-control form-control-user" style="border-radius: 0.5vw;" id="l016" name="l016" placeholder="Masukkan Target Mesin L016 (product/week)">
                </div>
                <div class="form-group">
                  <div class="text-s font-weight-bold text-uppercase text-primary mb-1">Mesin M003</div>
                  <input type="text" class="form-control form-control-user" style="border-radius: 0.5vw;" id="m003" name="m003" placeholder="Masukkan Target Mesin M002 (product/week)">
                </div>
                <div class="form-group">
                  <div class="text-s font-weight-bold text-uppercase text-primary mb-1">Mesin P002</div>
                  <input type="text" class="form-control form-control-user" style="border-radius: 0.5vw;" id="p002" name="p002" placeholder="Masukkan Target Mesin P002 (product/week)">
                </div>
                <hr>
                <input type="submit" class="btn btn-primary btn-user btn-block" name="simpan" value="OK" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-between">
    <div class="col-xl-2 col-lg-2 mb-4">
      <a href="oee-details.php" class="btn btn-danger btn-icon-split btn-lg btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text text-white" style="width: 100%">Back</span>
      </a>
    </div>
    <div class="col-xl-2 col-lg-2 mb-4">
      <a href="logout.php" class="btn btn-secondary btn-icon-split btn-lg btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-sign-out-alt"></i>
        </span>
        <span class="text text-white" style="width: 100%">Keluar</span>
      </a>
    </div>
  </div>
</div>
</div>

<?php include('footer.php') ?>
</div>
</div>


<!-- Bootstrap -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>
</body>

</html>
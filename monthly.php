<?php
$m = $_GET["m"];
if (strlen($m) != 4)
  header('Location: /');

$judulhalaman = $m . ' Monthly History ';
include("header.php");
?>
<div class="container-fluid">
  <!-- Main Content Area -->
  <div class="row">
    <!-- Monthly Table -->
    <div class="col-xl-12 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#monthlyTableArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Data</h6>
        </a>
        <div class="collapse show" id="monthlyTableArea">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="monthly-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th rowspan="2">Machine</th>
                    <th rowspan="2">Parameter (%)</th>
                    <th colspan="31">Day</th>
                  </tr>
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                    <th>24</th>
                    <th>25</th>
                    <th>26</th>
                    <th>27</th>
                    <th>28</th>
                    <th>29</th>
                    <th>30</th>
                    <th>31</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td rowspan="4">Mesin Honning (H05)</td>
                    <td>OEE</td>
                    <td>90</td>
                    <td>80</td>
                    <td>85</td>
                    <td>88</td>
                    <td>80</td>
                    <td>60</td>
                    <td>40</td>
                    <td>90</td>
                    <td>75</td>
                    <td>80</td>
                    <td>75</td>
                    <td>70</td>
                    <td>40</td>
                    <td>60</td>
                    <td>92</td>
                    <td>80</td>
                    <td>85</td>
                    <td>60</td>
                    <td>85</td>
                    <td>80</td>
                    <td>30</td>
                    <td>88</td>
                    <td>89</td>
                    <td>90</td>
                    <td>90</td>
                    <td>85</td>
                    <td>70</td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>35</td>
                  </tr>
                  <tr>
                    <td>Availability</td>
                    <td>90</td>
                    <td>80</td>
                    <td>85</td>
                    <td>88</td>
                    <td>80</td>
                    <td>60</td>
                    <td>40</td>
                    <td>90</td>
                    <td>75</td>
                    <td>80</td>
                    <td>75</td>
                    <td>70</td>
                    <td>40</td>
                    <td>60</td>
                    <td>92</td>
                    <td>80</td>
                    <td>85</td>
                    <td>60</td>
                    <td>85</td>
                    <td>80</td>
                    <td>30</td>
                    <td>88</td>
                    <td>89</td>
                    <td>90</td>
                    <td>90</td>
                    <td>85</td>
                    <td>70</td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>35</td>
                  </tr>
                  <tr>
                    <td>Performance</td>
                    <td>90</td>
                    <td>80</td>
                    <td>85</td>
                    <td>88</td>
                    <td>80</td>
                    <td>60</td>
                    <td>40</td>
                    <td>90</td>
                    <td>75</td>
                    <td>80</td>
                    <td>75</td>
                    <td>70</td>
                    <td>40</td>
                    <td>60</td>
                    <td>92</td>
                    <td>80</td>
                    <td>85</td>
                    <td>60</td>
                    <td>85</td>
                    <td>80</td>
                    <td>30</td>
                    <td>88</td>
                    <td>89</td>
                    <td>90</td>
                    <td>90</td>
                    <td>85</td>
                    <td>70</td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>35</td>
                  </tr>
                  <tr>
                    <td>Quality</td>
                    <td>90</td>
                    <td>80</td>
                    <td>85</td>
                    <td>88</td>
                    <td>80</td>
                    <td>60</td>
                    <td>40</td>
                    <td>90</td>
                    <td>75</td>
                    <td>80</td>
                    <td>75</td>
                    <td>70</td>
                    <td>40</td>
                    <td>60</td>
                    <td>92</td>
                    <td>80</td>
                    <td>85</td>
                    <td>60</td>
                    <td>85</td>
                    <td>80</td>
                    <td>30</td>
                    <td>88</td>
                    <td>89</td>
                    <td>90</td>
                    <td>90</td>
                    <td>85</td>
                    <td>70</td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                    <td>35</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- OEE Line Chart -->
    <div class="col-xl-12 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#oeeArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">OEE Chart</h6>
        </a>
        <div class="collapse show" id="oeeArea">
          <div class="card-body">
            <div class="chart-area">
              <canvas id="oee-chart"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Availability-Performance-Quality Chart -->
    <div class="col-xl-12 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#apqArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Availability-Performance-Quality Chart</h6>
        </a>
        <div class="collapse show" id="apqArea">
          <div class="card-body">
            <div class="chart-area">
              <canvas id="apq-chart"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
</div>

<?php include('footer.php') ?>
</div>
</div>


<!-- Bootstrap -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>

<!-- ChartJS -->
<script src="vendor/chart.js/Chart.min.js"></script>

<!-- Main OEE Chart -->
<script>
  // OEE Chart
  var oee = document.getElementById("oee-chart");
  var oeeChart = new Chart(oee, {
    type: 'line',
    data: {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
      datasets: [{
        label: "OEE",
        lineTension: 0,
        backgroundColor: "rgba(78, 115, 223, 0.05)",
        borderColor: "rgba(78, 115, 223, 1)",
        pointRadius: 3,
        pointBackgroundColor: "rgba(78, 115, 223, 1)",
        pointBorderColor: "rgba(78, 115, 223, 1)",
        pointHoverRadius: 3,
        pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
        pointHoverBorderColor: "rgba(78, 115, 223, 1)",
        pointHitRadius: 10,
        pointBorderWidth: 2,
        data: [90, 80, 85, 88, 80, 60, 40, 90, 75, 80, 75, 70, 40, 60, 92, 80, 85, 60, 85, 80, 30, 88, 89, 90, 90, 85, 70, 10, 20, 30, 35]
      }],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 6,
            padding: 10,
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        intersect: false,
        mode: 'index',
        caretPadding: 10
      }
      // "hover": {
      //     "animationDuration": 0
      // },
      // "animation": {
      //     "onComplete": function() {
      //         var chartInstance = this.chart,
      //             ctx = chartInstance.ctx;

      //         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
      //         ctx.textAlign = 'center';
      //         ctx.textBaseline = 'bottom';

      //         this.data.datasets.forEach(function(dataset, i) {
      //             var meta = chartInstance.controller.getDatasetMeta(i);
      //             meta.data.forEach(function(bar, index) {
      //                 var data = dataset.data[index] + '%';
      //                 ctx.fillText(data, bar._model.x, bar._model.y - 10);
      //             });
      //         });
      //     }
      // }
    }
  });

  // Availability-Performance-Quality Chart
  var apq = document.getElementById("apq-chart");
  var apqChart = new Chart(apq, {
    type: 'line',
    data: {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
      datasets: [{
          label: "Availability",
          lineTension: 0,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [90, 80, 85, 88, 80, 60, 40, 90, 75, 80, 75, 70, 40, 60, 92, 90, 80, 85, 88, 80, 60, 40, 89, 90, 90, 80, 85, 88, 80, 60, 65]
        },
        {
          label: "Performance",
          lineTension: 0,
          backgroundColor: "rgba(255, 159, 64, 0.05)",
          borderColor: "rgba(255, 159, 64, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(255, 159, 64, 1)",
          pointBorderColor: "rgba(255, 159, 64, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [90, 80, 85, 88, 80, 60, 40, 90, 75, 80, 75, 70, 40, 60, 92, 80, 85, 60, 85, 80, 30, 88, 80, 72, 85, 85, 70, 10, 20, 30, 35]
        },
        {
          label: "Quality",
          lineTension: 0,
          backgroundColor: "rgba(75, 192, 192, 0.05)",
          borderColor: "rgba(75, 192, 192, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(75, 192, 192, 1)",
          pointBorderColor: "rgba(75, 192, 192, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [90, 80, 85, 88, 80, 60, 40, 90, 75, 80, 75, 70, 40, 60, 92, 80, 85, 60, 85, 80, 30, 70, 40, 60, 92, 80, 85, 60, 20, 40, 50]
        }
      ],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 6,
            padding: 10,
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: true,
        position: 'bottom',
      },
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        intersect: false,
        mode: 'index',
        caretPadding: 10
      }
    }
  });
</script>
</body>

</html>
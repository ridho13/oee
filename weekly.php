<?php
$m = $_GET["m"];
if (strlen($m) != 4)
  header('Location: /');

$judulhalaman = $m . ' Weekly History ';
include("header.php");
?>
<div class="container-fluid">
  <!-- Main Content Area -->
  <div class="row">
    <!-- Weekly Table -->
    <div class="col-xl-6 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#weeklyTableArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Data</h6>
        </a>
        <div class="collapse show" id="weeklyTableArea">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="weekly-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th rowspan="2">Machine</th>
                    <th rowspan="2">Parameter (%)</th>
                    <th colspan="4">Week</th>
                  </tr>
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td rowspan="4">Mesin Honning (H05)</td>
                    <td>OEE</td>
                    <td>93.45</td>
                    <td>92.3</td>
                    <td>56.57</td>
                    <td>11.86</td>
                  </tr>
                  <tr>
                    <td>Availability</td>
                    <td>38.31</td>
                    <td>82.88</td>
                    <td>38.56</td>
                    <td>24.79</td>
                  </tr>
                  <tr>
                    <td>Performance</td>
                    <td>44.63</td>
                    <td>35.61</td>
                    <td>63.73</td>
                    <td>34.99</td>
                  </tr>
                  <tr>
                    <td>Quality</td>
                    <td>59.99</td>
                    <td>22.83</td>
                    <td>89.94</td>
                    <td>62.25</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- OEE Bar Chart -->
    <div class="col-xl-6 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#oeeArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">OEE</h6>
        </a>
        <div class="collapse show" id="oeeArea">
          <div class="card-body">
            <div class="chart-bar">
              <canvas id="oee-chart" style="display: block; width: 1037px; height: 320px;" width="1037" height="320" class="chartjs-render-monitor"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Availability Bar Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#availabilityArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Availability</h6>
        </a>
        <div class="collapse show" id="availabilityArea">
          <div class="card-body">
            <div class="chart-bar">
              <canvas id="availability-chart" style="display: block; width: 1037px; height: 320px;" width="1037" height="320" class="chartjs-render-monitor"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Performance Bar Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#performanceArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Performance</h6>
        </a>
        <div class="collapse show" id="performanceArea">
          <div class="card-body">
            <div class="chart-bar">
              <canvas id="performance-chart" style="display: block; width: 1037px; height: 320px;" width="1037" height="320" class="chartjs-render-monitor"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Quality Bar Chart -->
    <div class="col-xl-4 col-lg-7">
      <div class="card shadow mb-4">
        <a href="#qualityArea" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-primary">Quality</h6>
        </a>
        <div class="collapse show" id="qualityArea">
          <div class="card-body">
            <div class="chart-bar">
              <canvas id="quality-chart" style="display: block; width: 1037px; height: 320px;" width="1037" height="320" class="chartjs-render-monitor"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
</div>

<?php include('footer.php') ?>
</div>
</div>


<!-- Bootstrap -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>

<!-- ChartJS -->
<script src="vendor/chart.js/Chart.min.js"></script>

<!-- Main OEE Chart -->
<script>
  Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
  Chart.defaults.global.defaultFontColor = '#858796';

  function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function(n, prec) {
        var k = Math.pow(10, prec);
        return '' + Math.round(n * k) / k;
      };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }

  // OEE Chart
  var oee = document.getElementById("oee-chart");
  var oeeChart = new Chart(oee, {
    type: 'horizontalBar',
    data: {
      labels: ["7", "6", "5", "4", "3", "2", "1"],
      datasets: [{
        label: "Machine",
        minBarLength: 0,
        maxBarLength: 100,
        backgroundColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        data: [40, 60, 80, 88, 85, 80, 90],
      }],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
          maxBarThickness: 25,
        }],
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
            padding: 10,
            callback: function(value, index, values) {
              return number_format(value) + '%';
            }
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
            return datasetLabel + ': ' + number_format(tooltipItem.xLabel) + '%';
          }
        }
      },
      "hover": {
        "animationDuration": 0
      },
      "animation": {
        "onComplete": function() {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;

          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function(dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function(bar, index) {
              var data = dataset.data[index] + '%';
              ctx.fillText(data, bar._model.x + 20, bar._model.y + 7.5);
            });
          });
        }
      }
    }
  });

  // Availability Chart
  var availability = document.getElementById("availability-chart");
  var availabilityChart = new Chart(availability, {
    type: 'horizontalBar',
    data: {
      labels: ["7", "6", "5", "4", "3", "2", "1"],
      datasets: [{
        label: "Machine",
        minBarLength: 0,
        maxBarLength: 100,
        backgroundColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        data: [60, 40, 70, 75, 80, 75, 90],
      }],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
          maxBarThickness: 25,
        }],
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
            maxTicksLimit: 6,
            padding: 10,
            callback: function(value, index, values) {
              return number_format(value) + '%';
            }
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
            return datasetLabel + ': ' + number_format(tooltipItem.xLabel) + '%';
          }
        }
      },
      "hover": {
        "animationDuration": 0
      },
      "animation": {
        "onComplete": function() {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;

          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function(dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function(bar, index) {
              var data = dataset.data[index] + '%';
              ctx.fillText(data, bar._model.x + 20, bar._model.y + 7.5);
            });
          });
        }
      }
    }
  });

  // Performance Chart
  var performance = document.getElementById("performance-chart");
  var performanceChart = new Chart(performance, {
    type: 'horizontalBar',
    data: {
      labels: ["7", "6", "5", "4", "3", "2", "1"],
      datasets: [{
        label: "Machine",
        minBarLength: 0,
        maxBarLength: 100,
        backgroundColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        data: [60, 40, 70, 75, 80, 75, 90],
      }],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
          maxBarThickness: 25,
        }],
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
            maxTicksLimit: 6,
            padding: 10,
            callback: function(value, index, values) {
              return number_format(value) + '%';
            }
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
            return datasetLabel + ': ' + number_format(tooltipItem.xLabel) + '%';
          }
        }
      },
      "hover": {
        "animationDuration": 0
      },
      "animation": {
        "onComplete": function() {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;

          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function(dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function(bar, index) {
              var data = dataset.data[index] + '%';
              ctx.fillText(data, bar._model.x + 20, bar._model.y + 7.5);
            });
          });
        }
      }
    }
  });

  // Quality Chart
  var quality = document.getElementById("quality-chart");
  var qualityChart = new Chart(quality, {
    type: 'horizontalBar',
    data: {
      labels: ["7", "6", "5", "4", "3", "2", "1"],
      datasets: [{
        label: "Machine",
        minBarLength: 0,
        maxBarLength: 100,
        backgroundColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        data: [60, 40, 70, 75, 80, 75, 90],
      }],
    },
    options: {
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false
          },
          maxBarThickness: 25,
        }],
        xAxes: [{
          ticks: {
            min: 0,
            max: 100,
            maxTicksLimit: 6,
            padding: 10,
            callback: function(value, index, values) {
              return number_format(value) + '%';
            }
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
          label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
            return datasetLabel + ': ' + number_format(tooltipItem.xLabel) + '%';
          }
        }
      },
      "hover": {
        "animationDuration": 0
      },
      "animation": {
        "onComplete": function() {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;

          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function(dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function(bar, index) {
              var data = dataset.data[index] + '%';
              ctx.fillText(data, bar._model.x + 20, bar._model.y + 7.5);
            });
          });
        }
      }
    }
  });
</script>
</body>

</html>
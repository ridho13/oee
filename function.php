<?php
include("connection.php");

if (!isset($_SESSION['start']) && !isset($_SESSION['end'])) {
  $_SESSION['start'] = date('Y-m-d', strtotime('-7 days'));
  $_SESSION['end'] = date('Y-m-d');
}

function getBreakDown($machine, $language, $start = null, $end = null)
{
  global $con;
  global $arr;

  if (!$start && !$end) {
    $start = strtotime($_SESSION['start']);
    $end = strtotime($_SESSION['end']) + 86400;
  }

  $category = machineToCategory($machine);
  $plant = machineToPlant($machine);

  $sql = "SELECT `recover_time@timestamp` as time FROM " . $plant . "_event as event INNER JOIN " . $plant . "_event_log as event_log 
        ON event_log.event_log_index = event.event_log_index WHERE category = " . $category . "
        AND language1='" . $language . "'AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end ORDER BY `event`.`recover_time@timestamp` DESC LIMIT 1";


  $result = mysqli_query($con, $sql);
  if ($row = $result->fetch_assoc()) {
    $time = $row['time'];
    // echo $time;
    echo date('d-m-Y H:i', $time);
  }
}
function getTarget($mesin)
{
  global $con;

  $sql = "SELECT target FROM table_target WHERE mesin = '$mesin'";

  $result = mysqli_query($con, $sql);
  if ($row = $result->fetch_assoc()) {
    $jawaban = $row['target'];
    echo $jawaban;
  }
}

function availability($machine, $language, $start = null, $end = null)
{
  global $con;
  global $arr;

  if (!$start && !$end) {
    $start = strtotime($_SESSION['start']);
    $end = strtotime($_SESSION['end']) + 86400;
  }

  $category = machineToCategory($machine);
  $plant = machineToPlant($machine);

  //        $arr[$mesin] ."' AND  ORDER BY `event`.`trigger_time@timestamp` ";

  $sql = "SELECT sum(`recover_time@timestamp`-`trigger_time@timestamp`) as time FROM " . $plant . "_event as event INNER JOIN " .
    $plant . "_event_log as event_log ON event_log.event_log_index = event.event_log_index " .
    "WHERE `event`.`trigger_time@timestamp` is not NULL AND `event`.`recover_time@timestamp` is not NULL " .
    "AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end " .
    " AND category='" .  $category . "' AND language1='" . $language . "' ";

  //die($sql);

  $result = mysqli_query($con, $sql);
  if ($row = $result->fetch_assoc()) {
    $time = $row['time'];
    $final = intval($time);
    return $final;
  }
}
function quality($machine, $language, $start = null, $end = null)
{
  global $con;
  global $arr;

  if (!$start && !$end) {
    $start = strtotime($_SESSION['start']);
    $end = strtotime($_SESSION['end']) + 86400;
  }

  $category = machineToCategory($machine);
  $plant = machineToPlant($machine);

  //        $arr[$mesin] ."' AND  ORDER BY `event`.`trigger_time@timestamp` ";

  $sql = "SELECT count(`trigger_time@timestamp`) as time FROM " . $plant . "_event as event INNER JOIN " .
    $plant . "_event_log as event_log ON event_log.event_log_index = event.event_log_index " .
    "WHERE `event`.`trigger_time@timestamp` is not NULL AND `event`.`recover_time@timestamp` is not NULL " .
    "AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end " .
    " AND category='" .  $category . "' AND language1='" . $language . "' ";

  //die($sql);

  $result = mysqli_query($con, $sql);
  if ($row = $result->fetch_assoc()) {
    $time = $row['time'];
    // echo $time;
    return $time;
  }
}
function frekuensi($machine, $language, $start = null, $end = null)
{
  global $con;
  global $arr;

  if (!$start && !$end) {
    $start = strtotime($_SESSION['start']);
    $end = strtotime($_SESSION['end']) + 86400;
  }

  $category = machineToCategory($machine);
  $plant = machineToPlant($machine);

  $sql = "SELECT count(`recover_time@timestamp`-`trigger_time@timestamp`) as time FROM " . $plant . "_event as event INNER JOIN " .
    $plant . "_event_log as event_log ON event_log.event_log_index = event.event_log_index " .
    "WHERE `event`.`trigger_time@timestamp` is not NULL AND `event`.`recover_time@timestamp` is not NULL " .
    "AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end " .
    " AND category='" .  $category . "' AND language1='" . $language . "' ";

  $result = mysqli_query($con, $sql);
  if ($row = $result->fetch_assoc()) {
    $time = $row['time'];
    // echo $time;
    return $time;
  }
}

function fixedDate($time)
{
  $seconds = intval($time);
  $hours = floor($seconds / 3600);
  $mins = floor($seconds / 60 % 60);
  $secs = floor($seconds % 60);

  echo $hours . ':' . $mins . ':' . $secs;
}

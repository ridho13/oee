<?php
session_start();
$judulhalaman = 'Parameter (Weekly)';
include("header.php");
// include("connection.php");
include("function.php");

// function query($index) {
//     include("connection.php");
//     $sql = "SELECT `trigger_time@timestamp` as start, `recover_time@timestamp` as finish FROM `plant2_event` WHERE `event_log_index` = ".$index." ORDER BY `event_index` DESC LIMIT 1";
//     $result = mysqli_query($con,$sql);
//     if (mysqli_num_rows($result) == 0) {
//         return $data = NULL;
//     } else {

//         foreach ($result as $row) {
//             if ($row['finish'] != NULL) {
//                 $finish =  date("Y-m-d H:i:s ",$row['finish']);
//             } else {
//                 $row['finish'] = time();
//                 $finish = date("Y-m-d H:i:s ",$row['finish']);
//             }

//             if ($row['start'] != NULL) {
//                 $start =  date("Y-m-d H:i:s ",$row['start']);
//             } else {
//                 $row['start'] = time();
//                 $start = date("Y-m-d H:i:s ",$row['start']);
//             }

//         // $start = date("Y-m-d H:i:s ",$row['start']);
//         }
//     }
//     $data['start'] = $start;
//     $data['finish'] = $finish;
//     return $data;
// }
// return $mesin2 = query(11);
// $mesin1 = query(0;,4);

// return var_dump($mesin2);

// function function1($machine, $language)
// {
//   global $con;
//   global $arr;

//   $start = 0;
//   $end = 1607000174;
//   //        $start = $_SESSION['start'];
//   //        $end = $_SESSION['end'];

//   $category = machineToCategory($machine);
//   $plant = machineToPlant($machine);

//   //        $arr[$mesin] ."' AND  ORDER BY `event`.`trigger_time@timestamp` ";

//   $sql = "SELECT sum(`recover_time@timestamp`-`trigger_time@timestamp`) as time FROM " . $plant . "_event as event INNER JOIN " .
//     $plant . "_event_log as event_log ON event_log.event_log_index = event.event_log_index " .
//     "WHERE `event`.`trigger_time@timestamp` is not NULL AND `event`.`recover_time@timestamp` is not NULL " .
//     "AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end " .
//     " AND category='" .  $category . "' AND language1='" . $language . "' ";

//   //die($sql);

//   $result = mysqli_query($con, $sql);
//   if ($row = $result->fetch_assoc()) {
//     $time = $row['time'];
//     echo intval($time);
//   }
// }
// function functionCount($machine, $language)
// {
//   global $con;
//   global $arr;

//   $start = 0;
//   $end = 1607000174;
//   //        $start = $_SESSION['start'];
//   //        $end = $_SESSION['end'];

//   $category = machineToCategory($machine);
//   $plant = machineToPlant($machine);

//   //        $arr[$mesin] ."' AND  ORDER BY `event`.`trigger_time@timestamp` ";

//   $sql = "SELECT count(`trigger_time@timestamp`) as time FROM " . $plant . "_event as event INNER JOIN " .
//     $plant . "_event_log as event_log ON event_log.event_log_index = event.event_log_index " .
//     "WHERE `event`.`trigger_time@timestamp` is not NULL AND `event`.`recover_time@timestamp` is not NULL " .
//     "AND `trigger_time@timestamp`>=$start AND `recover_time@timestamp`<=$end " .
//     " AND category='" .  $category . "' AND language1='" . $language . "' ";

//   //die($sql);

//   $result = mysqli_query($con, $sql);
//   if ($row = $result->fetch_assoc()) {
//     $time = $row['time'];
//     echo $time;
//   }
// }

// function getBreakDown($machine, $language)
// {
//   global $con;
//   global $arr;
//   $start = 0;
//   $end = 1607000174;

//   $category = machineToCategory($machine);
//   $plant = machineToPlant($machine);

//   $sql = "SELECT `recover_time@timestamp` as time FROM " . $plant . "_event as event INNER JOIN " . $plant . "_event_log as event_log 
//         ON event_log.event_log_index = event.event_log_index WHERE category = " . $category . "
//         AND language1='" . $language . "' ORDER BY `event`.`recover_time@timestamp` DESC LIMIT 1";

//   // die($sql);

//   $result = mysqli_query($con, $sql);
//   if ($row = $result->fetch_assoc()) {
//     $time = $row['time'];
//     // echo $time;
//     echo date('d-m-Y H:i', $time);
//   }
// }
// function getTarget($mesin)
// {
//   global $con;

//   $sql = "SELECT target FROM table_target WHERE mesin = '$mesin'";

//   $result = mysqli_query($con, $sql);
//   if ($row = $result->fetch_assoc()) {
//     $jawaban = $row['target'];
//     echo $jawaban;
//   }
// }

?>
<div class="container-fluid">
  <!-- Main Content Area -->
  <?php include('date-picker.php') ?>
  <!-- error dari date picker nya  -->

  <!-- OEE History Table -->
  <div class="row justify-content-end">
    <div class="col-xl-12 col-lg-12">
      <div class="card shadow mb-4">
        <a class="d-block card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tabel Parameter Mingguan</h6>
        </a>
        <div class="collapse show" id="oeeHistoryTable">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="oee-history-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Machine Code</th>
                    <th>Run Time</th>
                    <th>Setup Time</th>
                    <th>Idle Time</th>
                    <th>Breakdown Time</th>
                    <th>Support Time</th>
                    <th>Actual Products</th>
                    <th>Target Products</th>
                    <th>"Good" Products</th>
                    <th>Last Breakdown Time</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>H005</td>
                    <td><?= fixedDate(availability("H005", "Run Time")); ?></td>
                    <td><?= fixedDate(availability("H005", "Setup Time")); ?></td>
                    <td><?= fixedDate(availability("H005", "Idle Time")); ?></td>
                    <td><?= fixedDate(availability("H005", "Breakdown Time")); ?></td>
                    <td><?= fixedDate(availability("H005", "Support Time")); ?></td>
                    <td><?= frekuensi("H005", "New Product Time"); ?></td>
                    <td><?= getTarget("H005") ?></td>
                    <td><?= frekuensi("H005", "Product Good Time"); ?></td>
                    <td><?= getBreakDown("H005", "Breakdown Time"); ?></td>
                  </tr>
                  <tr>
                    <td>L010</td>
                    <td><?= fixedDate(availability("L010", "Run Time")); ?></td>
                    <td><?= fixedDate(availability("L010", "Setup Time")); ?></td>
                    <td><?= fixedDate(availability("L010", "Idle Time")); ?></td>
                    <td><?= fixedDate(availability("L010", "Breakdown Time")); ?></td>
                    <td><?= fixedDate(availability("L010", "Support Time")); ?></td>
                    <td><?= frekuensi("L010", "New Product Time"); ?></td>
                    <td><?= getTarget("L010") ?></td>
                    <td><?= frekuensi("L010", "Product Good Time"); ?></td>
                    <td><?= getBreakDown("L010", "Breakdown Time"); ?></td>
                  </tr>
                  <tr>
                    <td>L016</td>
                    <td><?= fixedDate(availability("L016", "Run Time")); ?></td>
                    <td><?= fixedDate(availability("L016", "Setup Time")); ?></td>
                    <td><?= fixedDate(availability("L016", "Idle Time")); ?></td>
                    <td><?= fixedDate(availability("L016", "Breakdown Time")); ?></td>
                    <td><?= fixedDate(availability("L016", "Support Time")); ?></td>
                    <td><?= frekuensi("L016", "New Product Time"); ?></td>
                    <td><?= getTarget("L016") ?></td>
                    <td><?= frekuensi("L016", "Product Good Time"); ?></td>
                    <td><?= getBreakDown("L016", "Breakdown Time"); ?></td>
                  </tr>
                  <tr>
                    <td>M003</td>
                    <td><?= fixedDate(availability("M003", "Run Time")); ?></td>
                    <td><?= fixedDate(availability("M003", "Setup Time")); ?></td>
                    <td><?= fixedDate(availability("M003", "Idle Time")); ?></td>
                    <td><?= fixedDate(availability("M003", "Breakdown Time")); ?></td>
                    <td><?= fixedDate(availability("M003", "Support Time")); ?></td>
                    <td><?= frekuensi("M003", "New Product Time"); ?></td>
                    <td><?= getTarget("M003") ?></td>
                    <td><?= frekuensi("M003", "Product Good Time"); ?></td>
                    <td><?= getBreakDown("M003", "Breakdown Time"); ?></td>
                  </tr>
                  <tr>
                    <td>P002</td>
                    <td><?= fixedDate(availability("P002", "Run Time")); ?></td>
                    <td><?= fixedDate(availability("P002", "Setup Time")); ?></td>
                    <td><?= fixedDate(availability("P002", "Idle Time")) ?></td>
                    <td><?= fixedDate(availability("P002", "Breakdown Time")); ?></td>
                    <td><?= fixedDate(availability("P002", "Support Time")); ?></td>
                    <td><?= frekuensi("P002", "New Product Time"); ?></td>
                    <td><?= getTarget("P002") ?></td>
                    <td><?= frekuensi("P002", "Product Good Time"); ?></td>
                    <td><?= getBreakDown("P002", "Breakdown Time"); ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-between">
    <div class="col-xl-2 col-lg-2 mb-4">
      <a href="index.php" class="btn btn-danger btn-icon-split btn-lg btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text text-white" style="width: 100%">Back</span>
      </a>
    </div>
    <div class="col-xl-2 col-lg-2 mb-4">
      <a onclick="window.print()" class="btn btn-primary btn-icon-split btn-lg btn-block">
        <span class="icon text-white-50">
          <i class="fas fa-download"></i>
        </span>
        <span class="text text-white" style="width: 100%">Print</span>
      </a>
    </div>
  </div>
</div>
</div>

<!-- <?php //include('footer.php') 
      ?> -->
</div>
</div>


<!-- Scroll to Top -->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Internal plugin JavaScript -->
<script src="js/sb-admin-2.min.js"></script>

<!-- ChartJS -->
<script src="vendor/chart.js/Chart.min.js"></script>
</body>

</html>